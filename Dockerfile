FROM centos:7

MAINTAINER Dan Levy <dan@danlevy.net>

# Install base dev deps
RUN yum -y install libaio && \
	yum -y groupinstall 'Development Tools'

RUN rpm -ivh https://rpm.nodesource.com/pub_0.12/el/7/x86_64/nodejs-0.12.7-1nodesource.el7.centos.x86_64.rpm
COPY *.rpm /tmp/
RUN rpm -ivh /tmp/oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm
RUN rpm -ivh /tmp/oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm

RUN mkdir /app
WORKDIR /app

ADD startup.sh /
RUN /startup.sh

EXPOSE 22
EXPOSE 8080
EXPOSE 3000

CMD ["bash"]

